<?php
require_once '../vendor/autoload.php';

use Phalcon\Di\FactoryDefault;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {

    /**
     * The FactoryDefault Dependency Injector automatically registers
     * the services that provide a full stack framework.
     */
    $di = new FactoryDefault();

    $app = new \Phalcon\Mvc\Micro();
    $app->setDI($di);

    /**
     * Handle routes
     */
    include APP_PATH . '/config/router.php';

    /**
     * Read services
     */
    include APP_PATH . '/config/services.php';

    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();

    /**
     * Include Autoloader
     */
    include APP_PATH . '/config/loader.php';

    $app->after(
        function () use ($app) {
            $return = $app->getReturnedValue();

            if (is_array($return)) {
                $app->response->setContent(json_encode($return));
            } else {
                throw new Exception('Bad Response');
            }
            $app->response->send();
        }
    );

    // Processing request
    $app->handle($_SERVER['REQUEST_URI']);

} catch (\Phalcon\Exception $e) {
    $app->response->setStatusCode($e->getCode(), $e->getMessage())
        ->setJsonContent([
            'message' => $e->getMessage()
        ])
        ->send();

} catch (\Exception $e) {
    $app->response->setStatusCode(500, 'Internal Server Error')
        ->setJsonContent([
            'message' => 'Some error occurred on the server.'
        ])
        ->send();
}
