<?php

class UserController extends ControllerBase
{
    /**
     * Endpoint for saving username to DB
     *
     * @return array
     */
    public function addAction()
    {
        $username = $this->request->getPost('username');

        $this->userService->addUsernameToDatabase($username);

        return [
            'result' => 'OK'
        ];
    }

    /**
     * Endpoint for retrieving last tweets (timeline) for saved usernames
     */
    public function getTimelinesAction()
    {
        $this->userService->getTimelinesByUsernames();
    }

    /**
     * Endpoint for deleting username from DB
     *
     * @param string $username
     *
     * @return array
     */
    public function deleteAction(string $username)
    {
        $this->userService->deleteUsernameFromDatabase($username);

        return [
            'result' => 'OK'
        ];
    }
}