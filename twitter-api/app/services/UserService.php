<?php

class UserService
{
    private $twitterApiService;

    public function __construct(TwitterApiService $twitterApiService)
    {
        $this->twitterApiService = $twitterApiService;
    }
    
    public function addUsernameToDatabase(string $username)
    {
        $user = new Users();
        $user->name = $username;

        $user->save();
    }

    public function deleteUsernameFromDatabase(string $username)
    {
        $user = Users::findFirst([
            'name LIKE ' . '\'' . $username . '\''
        ]);

        $user->delete();
    }

    public function getTimelinesByUsernames()
    {
        return $this->twitterApiService->getUsersTimelines('TechCrunch');
    }
}