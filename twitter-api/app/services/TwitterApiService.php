<?php

class TwitterApiService
{
    private $appConfig;

    private $httpClient;

    private $baseUrl = 'https://api.twitter.com/1.1/';

    public function __construct(\Phalcon\Config $config)
    {
        $this->appConfig = $config;
        $this->httpClient = new \GuzzleHttp\Client();
    }

    public function getUsersTimelines(string $screenName)
    {
        $params = [
            'method' => 'GET',
            'apiUrl' => 'statuses/user_timeline.json',
            'query'  => 'screen_name=' . $screenName . '&count=10'
        ];

        $result = $this->httpClient->request(
            $params['method'],
            $this->baseUrl.$params['apiUrl'] . '?' . $params['query'],
            [
                'headers' => [
                    'Authorization' => $this->getAuthHeader($params)
                ]
            ]);

        return $result;
    }

    public function getAuthHeader(array $params)
    {
        $timestamp = time();
        $nonce = $this->generateNonce();
        $signature = $this->generateSignature(
            $params['method'],
            $params['apiUrl'],
            $params['query'],
            $timestamp,
            $nonce
        );

        $headerParams = [
            'oauth_consumer_key="' . $this->appConfig->twitterConsumerKey . '"',
            'oauth_nonce="' . $nonce . '"',
            'oauth_signature="' . $signature . '"',
            'oauth_signature_method="HMAC-SHA1"',
            'oauth_timestamp="' . $timestamp . '"',
            'oauth_token="' . $this->appConfig->twitterAccessToken . '"',
            'oauth_version="1.0"'
        ];

        return 'OAuth ' . implode(', ', $headerParams);
    }

    public function generateSignature(string $method, string $apiUrl, string $query, int $timestamp, string $nonce)
    {
        $parameters = [
            rawurlencode('oauth_consumer_key=' . $this->appConfig->twitterConsumerKey),
            rawurlencode('oauth_nonce=' . $nonce),
            rawurlencode('oauth_signature_method=HMAC-SHA1'),
            rawurlencode('oauth_timestamp=' . $timestamp),
            rawurlencode('oauth_token=' . $this->appConfig->twitterAccessToken),
            rawurlencode('oauth_version=1.0'),
            rawurlencode($query)
        ];

        $baseString = implode('&', $parameters);
        $baseString = $method . '&' . rawurlencode($this->baseUrl.$apiUrl) . '&' . rawurlencode($baseString);

        $signingKey = rawurlencode($this->appConfig->twitterConsumerSecret) . '&' . rawurlencode($this->appConfig->twitterAccessSecret);

        $signature = base64_encode(hash_hmac("sha1", $baseString, $signingKey, true));

        return rawurlencode($signature);
    }

    public function generateNonce(int $length = 42)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}