<?php

$router = $di->getRouter();

// Define your routes here

$usersCollection = new \Phalcon\Mvc\Micro\Collection();
$usersCollection->setHandler('UserController', true);
$usersCollection->setPrefix('/user');
$usersCollection->post('/add', 'addAction');
$usersCollection->get('/getTimelines', 'getTimelinesAction');
$usersCollection->delete('/{userName}', 'deleteAction');
$app->mount($usersCollection);

$app->notFound(
    function () use ($app) {
        throw new \Phalcon\Exception('Method not found', 404);
    }
);
